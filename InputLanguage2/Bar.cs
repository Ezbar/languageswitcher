﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Runtime.InteropServices;

namespace InputLanguage2
{
    public partial class Bar : Form
    {
        // Show a Form without stealing focus
        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        public Bar()
        {
            InitializeComponent();

            langTitles = new List<string>();
            langCodes = new List<string>();
            foreach (InputLanguage lang in InputLanguage.InstalledInputLanguages)
            {                
                langTitles.Add(lang.Culture.Parent.NativeName);
                langCodes.Add(lang.Culture.ToString());
            }

            pos = langCodes.IndexOf(InputLanguage.CurrentInputLanguage.Culture.ToString()); // текущий язык
            if (pos < langCodes.Count()-1) prev_index = pos + 1; else prev_index = 0; // прошлый язык будет следующим в списке
        }

        int counter = 0;
        int prev_index = -1;
        int pos = 0;

        string retHex;

        List<string> langTitles;
        List<string> langCodes;        
                
        public string getHex()
        {
            return retHex;
        }

        public void DoHide()
        {
            int intValue = new CultureInfo(langCodes[pos]).KeyboardLayoutId;
            string hex = intValue.ToString("X8");            
            retHex = hex;            
            counter = 0;
            Hide();
        }

        public void SetLanguage(string langName)
        {   
            counter++;

            lblLanguage.Text = InputLanguage.CurrentInputLanguage.Culture.Parent.NativeName;

            if (counter == 1) // берем предыдущий
            {
                int t = prev_index;
                prev_index = pos;
                pos = t;
            }
            else // если нажали второй раз
            {
                prev_index = pos;                
                if (pos > 0)  pos = pos - 1; else pos = langTitles.Count()-1;
            }
            lblLanguage.Text = langTitles[pos];
        }
    }
}
